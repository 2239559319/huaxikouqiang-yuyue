import axios from 'axios';
import cheerio from 'cheerio';
import fs from 'fs';
import { join } from 'path';

const configDir = join(__dirname, '../config');
const logDir = join(__dirname, '../log');

const cookie = fs.readFileSync(join(configDir, 'cookie.txt'), 'utf-8');

export const instance = axios.create({
  baseURL: 'http://his.mobimedical.cn',
  headers: {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 NetType/WIFI MicroMessenger/7.0.20.1781(0x6700143B) WindowsWechat(0x6309091b) XWEB/8555 Flue',
    'Cookie': cookie
  },
});

export async function getDoctorList(date, url) {
  const mergeUrl = `${url}&date=${date}`;
  const res = await instance.get(mergeUrl);
  const text = await res.data;

  const $ = cheerio.load(text);

  const list = $('.doctorList .register-btn').filter((i, el) => !!$(el).attr('data-scheduleid'));
  const scheduleidList = list.map((i, el) => $(el).attr('data-scheduleid')).get();

  console.log(`scheduleidList length ${scheduleidList.length}`);

  return scheduleidList;
}

export async function submit(scheduleId) {
  const url = '/index.php?g=Weixin&m=CloudRegisterOne&a=getSchedulePeriod';
  const data = new URLSearchParams({
    scheduleId,
  });
  const res = await instance.post(url, data.toString(), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  });
  const respData = await res.data;
  const d = (new Date()).toString();
  fs.writeFileSync(join(logDir, d, '.txt'), JSON.stringify(respData), 'utf-8');
}
