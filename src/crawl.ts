import { join } from "path";
import { getDoctorList, submit } from "./api";
const urls = require(join(__dirname, "../config/dep.json"));

(async () => {
  const dateList = [
    "2024-03-05",
    "2024-03-06",
    "2024-03-07",
    "2024-03-08",
    "2024-03-09",
    "2024-03-10",
    "2024-03-11",
  ];

  const url =
    "/index.php?g=Weixin&m=CloudRegisterOne&a=three&deptHisId=086028000A000013&regType=0&wx=MbTXAN0k";
  for (const date of dateList) {
    const docList = await getDoctorList(date, url);
    if (docList.length) {
      await submit(docList[0]);
    }
  }
})();
